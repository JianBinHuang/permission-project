package com.rbac;

import com.rbac.entity.Permission;
import com.rbac.service.IPermissionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class DemoApplicationTests {

    @Autowired
    IPermissionService permissionService;

    @Test
    void contextLoads() {

        List<Permission> list = permissionService.list();
        System.out.println(list);
    }

}
