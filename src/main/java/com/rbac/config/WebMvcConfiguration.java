package com.rbac.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Author JianBinHuang
 * @Date 2021/9/24 20:04
 * @Description
 */
@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {

    /**
     * @Author JianBinHuang
     * @Date 2021/9/28 17:26
     * @param
     * @return
     * @Description
     * 添加 跨域拦截器
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {

        registry
                .addMapping("/**")
                .allowedHeaders("*")
                .allowedMethods("GET","POST","DELETE","PUT")
                .allowedOrigins("*");
    }
}
