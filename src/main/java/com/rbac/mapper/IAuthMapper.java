package com.rbac.mapper;

import com.rbac.entity.Permission;
import com.rbac.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author JianBinHuang
 * @Date 2021/9/28 11:17
 * @Description
 */
@Mapper
public interface IAuthMapper {
    /**
     * 取消角色用户权限
     * @param uid
     * @param rid
     * @return
     */
    Boolean unAuthRoleUser(@Param("uid") Integer uid, @Param("rid") Integer rid);

    /**
     * 添加角色用户权限
     * @param roleId
     * @param users
     */
    void authRoleUser(@Param("roleId") Integer roleId, @Param("users") List<User> users);

    /**
     * 删除重复的选中项
     * @param roleId
     * @param users
     */
    void batchUnAuthRoleUser(@Param("roleId") Integer roleId, @Param("users") List<User> users);

    /**
     * 获取选中角色自己的menu
     * @param roleId
     * @return
     */
    List<Integer> getRoleMenuList(Integer roleId);

    /**
     * 删除重复的权限菜单
     * @param roleId
     * @param menuIdList
     */
    void batchUnAuthRoleMenu(@Param("roleId") Integer roleId, @Param("menuIdList") List<Integer> menuIdList);

    /**
     * 添加角色权限菜单
     * @param roleId
     * @param menuIdList
     */
    void authRoleMenu(@Param("roleId") Integer roleId, @Param("menuIdList") List<Integer> menuIdList);
}
