package com.rbac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rbac.entity.User;

import java.util.List;

/**
 * @Author JianBinHuang
 * @Date 2021/9/24 11:30
 * @Description
 */
public interface UserMapper extends BaseMapper<User> {
    /**
     * 通过角色id获取用户数据
     * @param roleId
     * @return
     */
    List<User> getUserByRoleId(Integer roleId);
}
