package com.rbac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rbac.entity.Permission;

import java.util.List;

/**
 * @Author JianBinHuang
 * @Date 2021/9/26 12:00
 * @Description
 */
public interface PermissionMapper extends BaseMapper<Permission> {
    /**
     * 展示所有树形表(简单树)
     * @return
     */
    List<Permission> treeList();
}
