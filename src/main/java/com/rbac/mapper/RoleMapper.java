package com.rbac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rbac.entity.Role;

/**
 * @Author JianBinHuang
 * @Date 2021/9/26 12:00
 * @Description
 */
public interface RoleMapper extends BaseMapper<Role> {
}
