package com.rbac.common.uils;

import java.util.HashMap;

/**
 * @Author JianBinHuang
 * @Date 2021/9/24 11:32
 * @Description
 */
public class R extends HashMap<String,Object> {

    public R() {
        //code 为0代表成功
        super.put("code", 0);
        super.put("msg","success");
    }

    /**
     * @Author JianBinHuang
     * @Date 2021/9/24 11:41
     * @param
     * @return
     * @Description 响应成功
     */
    public static R ok(){

        return new R();
    }


    /**
     * @Author JianBinHuang
     * @Date 2021/9/24 11:41
     * @param
     * @return
     * @Description 响应成功带数据
     */
    public static R ok(String msg){

        R r=new R();
        r.put("msg", msg);
        return r;
    }

    /**
     * @Author JianBinHuang
     * @Date 2021/9/24 11:44
     * @param
     * @return
     * @Description
     *  考虑到返回值的 类型 所以传object
     */
    public static R out(Object flag){
        if(flag instanceof Boolean){
            if((Boolean) flag){
                return R.ok();
            }else{
                return R.error();
            }
        }else if(flag instanceof Integer){
            if((Integer)flag>0){
                return R.ok();
            }else{
                return R.error();
            }
        }
        return R.error();
    }

    /**
     * @Author JianBinHuang
     * @Date 2021/9/24 11:41
     * @param
     * @return
     * @Description 响应失败
     */
    public static R error(){

        return new R();
    }

    public static R error(String msg){
        R r=new R();
        r.put("code", 500);
        r.put("msg", msg);
        return r;
    }

    public static R putData(String key,Object value){
        R r=new R();
        r.put(key, value);
        return r;
    }
}
