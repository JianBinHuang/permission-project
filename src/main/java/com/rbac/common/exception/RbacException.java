package com.rbac.common.exception;

import lombok.Data;

/**
 * @Author JianBinHuang
 * @Date 2021/9/24 11:56
 * @Description
 */

@Data
public class RbacException extends RuntimeException{

    private Integer code;

    private String msg;
}
