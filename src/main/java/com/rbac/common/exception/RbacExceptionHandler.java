package com.rbac.common.exception;

import com.rbac.common.uils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Author JianBinHuang
 * @Date 2021/9/24 11:34
 * @Description
 */
@RestControllerAdvice
@Slf4j
public class RbacExceptionHandler{


    /**
     * @Author JianBinHuang
     * @Date 2021/9/28 17:27
     * @param
     * @return
     * @Description
     * 添加系统异常
     */
    @ExceptionHandler(Exception.class)
    public R systemException(Exception e){
        log.error("这是系统异常",e);
        return R.error();
    }

    /**
     * @Author JianBinHuang
     * @Date 2021/9/28 17:27
     * @param
     * @return
     * @Description
     * 添加自定义异常
     */
    @ExceptionHandler(RbacException.class)
    public R rbacException(RbacException e){
        log.error("这是rbac异常",e);
        R error=R.error();
        error.put("msg", e.getMsg());
        return error;
    }

}
