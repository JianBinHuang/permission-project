package com.rbac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rbac.entity.Permission;

import java.util.List;

/**
 * @Author JianBinHuang
 * @Date 2021/9/26 12:01
 * @Description
 */
public interface IPermissionService extends IService<Permission> {

    /**
     * 展示所有树形表(简单树)
     * @return
     */
    List<Permission> treeList();
}
