package com.rbac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rbac.entity.User;

import java.util.List;

/**
 * @Author JianBinHuang
 * @Date 2021/9/24 11:31
 * @Description
 */
public interface IUserService extends IService<User> {

    /**
     * 获取用户的roleId
     * @param roleId
     * @return
     */
    List<User> getUserByRoleId(Integer roleId);
}
