package com.rbac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rbac.entity.Permission;
import com.rbac.mapper.PermissionMapper;
import com.rbac.service.IPermissionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author JianBinHuang
 * @Date 2021/9/26 12:02
 * @Description
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements IPermissionService {



    @Override
    public List<Permission> treeList() {
        return baseMapper.treeList();
    }
}
