package com.rbac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rbac.entity.Role;
import com.rbac.mapper.RoleMapper;
import com.rbac.service.IRoleService;
import org.springframework.stereotype.Service;

/**
 * @Author JianBinHuang
 * @Date 2021/9/26 12:02
 * @Description
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {
}
