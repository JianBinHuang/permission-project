package com.rbac.service.impl;

import com.rbac.entity.Permission;
import com.rbac.entity.User;
import com.rbac.mapper.IAuthMapper;
import com.rbac.service.IAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author JianBinHuang
 * @Date 2021/9/28 11:16
 * @Description
 */
@Service
public class AuthServiceImpl implements IAuthService {

    @Autowired
    private IAuthMapper authMapper;

    @Override
    public Boolean unAuthRoleUser(Integer uid, Integer rid) {
        return authMapper.unAuthRoleUser(uid,rid);
    }

    @Override
    public void authRoleUser(Integer roleId, List<User> users) {

        /**
         * 模块需求：
         * 因为选中添加的时候会添加进重复的数据，
         * 所以这个区域就是做去重复添加的
         */

        //1.先删除当前用户的角色
         authMapper.batchUnAuthRoleUser(roleId,users);


        //2.授权用户的角色

        authMapper.authRoleUser(roleId,users);
    }

    @Override
    public List<Integer> getRoleMenuList(Integer roleId) {
        return authMapper.getRoleMenuList(roleId);
    }

    @Override
    public void authRoleMenu(Integer roleId, List<Integer> menuIdList) {
        //1.删除当前角色拥有的菜单

        authMapper.batchUnAuthRoleMenu(roleId,menuIdList);

        //2.给当前角色赋予菜单
        authMapper.authRoleMenu(roleId,menuIdList);
    }
}
