package com.rbac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rbac.entity.User;
import com.rbac.mapper.UserMapper;
import com.rbac.service.IUserService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author JianBinHuang
 * @Date 2021/9/24 11:31
 * @Description
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    @Override
    public List<User> getUserByRoleId(Integer roleId) {
        return baseMapper.getUserByRoleId(roleId);
    }
}
