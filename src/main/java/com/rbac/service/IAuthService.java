package com.rbac.service;

import com.rbac.entity.Permission;
import com.rbac.entity.User;

import java.util.List;

/**
 * @Author JianBinHuang
 * @Date 2021/9/28 11:15
 * @Description
 */
public interface IAuthService {
    /**
     * 取消用户角色权限
     * @param uid
     * @param rid
     * @return
     */
    Boolean unAuthRoleUser(Integer uid, Integer rid);

    /**
     * 多选选中用户
     * @param roleId
     * @param users
     */
    void authRoleUser(Integer roleId, List<User> users);

    List<Integer> getRoleMenuList(Integer roleId);

    void authRoleMenu(Integer roleId, List<Integer> menuIdList);
}
