package com.rbac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rbac.entity.Role;

/**
 * @Author JianBinHuang
 * @Date 2021/9/26 12:01
 * @Description
 */
public interface IRoleService extends IService<Role> {
}
