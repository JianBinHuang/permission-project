package com.rbac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rbac.common.uils.R;
import com.rbac.entity.Role;
import com.rbac.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author xiaogongjue
 * @Date2021/9/26 15:48
 * @Description
 */

@RestController
@RequestMapping("/role")
public class RoleController {


    @Autowired
    private IRoleService roleService;

    /**
     * @Author xiaogongjue
     * @Date2021/9/26 15:57
     * @Description 分页
     */
    @PostMapping("/page")
    public R page(@RequestBody Page<Role> rolePage) {
        Page<Role> page = roleService.page(rolePage);
        return R.putData("data", page);

    }

    /**
     * @Author JianBinHuang
     * @Date 2021/9/28 17:23
     * @param
     * @return
     * @Description
     * 添加角色数据
     */
    @PostMapping("/save")
    public R save(@RequestBody Role role) {
        boolean b = roleService.save(role);
        return R.out(b);
    }

    /**
     * @Author JianBinHuang
     * @Date 2021/9/28 17:24
     * @param
     * @return
     * @Description
     * 删除
     */
    @GetMapping("/delete/{id}")
    public R delete(@PathVariable Integer id){

        boolean b = roleService.removeById(id);
        return R.out(b);
    }
    /**
     * @Author JianBinHuang
     * @Date 2021/9/28 17:24
     * @param
     * @return
     * @Description
     * 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody Role role){
        boolean b = roleService.updateById(role);
        return R.out(b);

    }
    /**
     * @Author JianBinHuang
     * @Date 2021/9/28 17:24
     * @param
     * @return
     * @Description
     * 获取单个数据
     */
    @GetMapping("/getById/{id}")
    public R getById(@PathVariable Integer id){
        Role role = roleService.getById(id);
        return R.putData("role",role);

    }

}
