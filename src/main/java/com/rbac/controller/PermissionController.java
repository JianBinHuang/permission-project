package com.rbac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rbac.common.uils.R;
import com.rbac.entity.Permission;
import com.rbac.entity.User;
import com.rbac.service.IPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author JianBinHuang
 * @Date 2021/9/26 15:32
 * @Description
 */
@RestController
@RequestMapping("/Per")
public class PermissionController {

    @Autowired
    IPermissionService permissionService;

    @GetMapping("/treeList")
    public R treeList() {
        //1.查询所有的数据
        //后端返回的是简单的json格式
        return R.putData("data", permissionService.treeList());
    }

    /**
     * @param
     * @return
     * @Author JianBinHuang
     * @Date 2021/9/26 16:20
     * @Description
     */
    @RequestMapping("/show")
    public R showPermission() {
        List<Permission> list = permissionService.list();
        return R.putData("data", list);
    }

    /**
     * @param
     * @return
     * @Author JianBinHuang
     * @Date 2021/9/26 16:20
     * @Description
     */
    @PostMapping("/PermissionPage")
    public R getPageList(@RequestBody Page<Permission> page) {
        page = permissionService.page(page);
        return R.putData("page", page);
    }

    /**
     * @param
     * @return
     * @Author JianBinHuang
     * @Date 2021/9/26 16:20
     * @Description
     */
    @PostMapping("/PreSave")
    public R PermissionSave(@RequestBody Permission permission) {
        boolean save = permissionService.save(permission);
        return R.out(save);
    }

    /**
     * @param
     * @return
     * @Author JianBinHuang
     * @Date 2021/9/26 16:39
     * @Description
     */
    @PostMapping("/PreUpdate")
    public R PermissionUpdate(@RequestBody Permission permission) {
        boolean b = permissionService.updateById(permission);
        return R.out(b);
    }

    /**
     * @param
     * @return
     * @Author JianBinHuang
     * @Date 2021/9/26 16:46
     * @Description
     */
    @GetMapping("/PreDelete/{id}")
    public R PermissionDeleteById(@PathVariable Integer id) {
        boolean b = permissionService.removeById(id);
        return R.out(b);
    }

    /**
     * @param
     * @return
     * @Author JianBinHuang
     * @Date 2021/9/26 16:46
     * @Description
     */
    @GetMapping("/PreGetById/{id}")
    public R PermissionGetById(@PathVariable Integer id) {
        //查询自己的数据
        Permission byId = permissionService.getById(id);
        //查询pid数据
        Permission pPermission = permissionService.getById(byId.getPid());
        //设置pname
        byId.setPname(pPermission.getName());
        return R.putData("data", byId);
    }
}
