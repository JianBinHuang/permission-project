package com.rbac.controller;

import com.rbac.common.uils.R;
import com.rbac.entity.Permission;
import com.rbac.entity.User;
import com.rbac.service.IAuthService;
import com.rbac.service.IPermissionService;
import com.rbac.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author JianBinHuang
 * @Date 2021/9/28 10:41
 * @Description
 */
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private IUserService userService;

    @Autowired
    private IAuthService authService;

    @Autowired
    private IPermissionService permissionService;

    /**
     * @Author JianBinHuang
     * @Date 2021/9/28 17:03
     * @param
     * @return
     * @Description
     */
    @PostMapping("/unAuthRoleUser/{uid}/{rid}")
    public R unAuthRoleUser(@PathVariable Integer uid,@PathVariable Integer rid){

       Boolean flag= authService.unAuthRoleUser(uid,rid);
        return R.out(flag);
    }


    /**
     * @Author JianBinHuang
     * @Date 2021/9/28 17:03
     * @param
     * @return
     * @Description
     */
    @GetMapping("/getUserByRoleId/{roleId}")
    public R getUserByRoleId(@PathVariable Integer roleId){

       List<User> userList=userService.getUserByRoleId(roleId);
        return R.putData("data", userList);
    }

    /**
     * @Author JianBinHuang
     * @Date 2021/9/28 16:53
     * @param
     * @return
     * @Description
     * 前端多选提交
     */
    @PostMapping("/authRoleUser/{roleId}")
    public R authRoleUser(@PathVariable Integer roleId,@RequestBody List<User> users){
        System.out.println("roleId = " + roleId + ", users = " + users);
        authService.authRoleUser(roleId,users);
        return R.ok();
    }

    /**
     * @Author JianBinHuang
     * @Date 2021/9/28 18:00
     * @param
     * @return
     * @Description
     *
     * 角色授权菜单
     */
    @GetMapping("/queryRoleMenuList/{roleId}")
    public R queryRoleMenuList(@PathVariable Integer roleId){

        //1.查询所有的菜单
        List<Permission> menuList = permissionService.list();

        //2.查询指定角色拥有的菜单
        List<Integer> roleMenuList= authService.getRoleMenuList(roleId);

        R r=R.ok();
        r.put("menuList", menuList);
        r.put("roleMenuList", roleMenuList);
       return r;
    }
    /**
     * @Author JianBinHuang
     * @Date 2021/9/28 19:43
     * @param
     * @return
     * @Description
     * 添加授权项
     */
    @PostMapping("/authRoleMenu/{roleId}")
    public R authRoleMenu(@PathVariable Integer roleId,@RequestBody List<Integer> menuIdList){
        authService.authRoleMenu(roleId,menuIdList);
        return R.ok();
    }
}
