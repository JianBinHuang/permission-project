package com.rbac.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author JianBinHuang
 * @Date 2021/9/24 11:23
 * @Description
 */
@Data
public class User implements Serializable {


    private Integer id;

    private String username;

    private String password;

    private Integer sex;

//    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date birthday;

    private Integer age;

    @TableField(select = true,fill = FieldFill.INSERT)
    private Integer status;

    private Integer createUser;

    @TableField(select = true,fill = FieldFill.INSERT)
    private Date createTime;

    private Integer updateUser;

    private Date updateTime;
}
