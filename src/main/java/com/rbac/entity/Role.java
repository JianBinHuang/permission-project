package com.rbac.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author JianBinHuang
 * @Date 2021/9/26 11:56
 * @Description
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Role implements Serializable {

    private Integer id;

    private String roleName;

    private String roleDesc;

    private String roleStatus;


}
