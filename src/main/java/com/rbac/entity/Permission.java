package com.rbac.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author JianBinHuang
 * @Date 2021/9/26 11:58
 * @Description
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Permission implements Serializable {

    private Integer id;

    private String name;

    private String url;

    private Integer type;

    private Integer status;

    private Integer pid;

    /**
     * @Author JianBinHuang
     * @Date 2021/9/27 19:32
     * @param
     * @return
     * @Description 告诉mybatis-plus 这个字段不从数据库表查询
     */
    @TableField(exist = false)
    private String pname;
}
