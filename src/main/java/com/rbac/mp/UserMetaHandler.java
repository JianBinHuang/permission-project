package com.rbac.mp;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Author JianBinHuang
 * @Date 2021/9/24 17:24
 * @Description
 */
@Component
public class UserMetaHandler implements MetaObjectHandler {

    /**
     * @Author JianBinHuang
     * @Date 2021/9/28 17:26
     * @param
     * @return
     * @Description
     * 添加用户的 默认状态值
     * 跟默认创建时间
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        Object status = getFieldValByName("status", metaObject);
        Object createTime = getFieldValByName("createTime", metaObject);

        if(status==null){
            setFieldValByName("status", 1, metaObject);
        }
        if(createTime==null){
            setFieldValByName("createTime", new Date(), metaObject);
        }

    }

    @Override
    public void updateFill(MetaObject metaObject) {

    }
}
